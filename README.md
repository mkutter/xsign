# Test des CA Cross Signings

Test für die beiden CA-Arbeitsgruppen im Fachpraktikum 1599 im Sommersemester
2016 an der FernUniversität in Hagen.

Technischer Hintergrung in [Cross-Signing](XSigning.md)

Ein Skript für den Test liegt in [xsign.sh](xsign.sh)

Es erzeugt Zertifikate entlang folgender Hierarchien:

   * Root CA 1 (```root1.pem```) - Int CA 1 (```int1.pem```) - Crt 1 (```crt1.pem```)
   * Root CA 2 (```root2.pem```) - Int CA 2 (```int2.pem```) - Crt 2 (```crt2.pem```)
   * Root CA 1 (```root1.pem```) - Root CA 2 (```root2x.pem```)
   * Root CA 2 (```root2.pem```) - Root CA 1 (```root1x.pem```)

Das Skript prüft, ob die Zertifikate ```crt1.pem``` und ```crt2.pem``` mit 
folgenden Chains verifiziert werden können:

   * ```root1.pem```, ```int1.pem```, ```root2x.pem```, ```int2.pem```
   * ```root2.pem```, ```int2.pem```, ```root1x.pem```, ```int1.pem```