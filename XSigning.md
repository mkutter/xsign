# Cross Signing

Dieses Dokument beschreibt das Direct Cross-Certification Model aus 
https://tools.ietf.org/html/rfc5217

Die Validierung von Zertifikaten ist entsprechend des Algorithmus aus 
https://tools.ietf.org/html/rfc5280#section-6.1 (vereinfacht) beschrieben.

## Cross-Signing der Root-Zertifikate

Bei einem direkten Cross-Signing stellen sich die Beziehungen
zwischen den Zertifikaten so dar (Pfeile bedeuten "ist signiert von", oder 
technisch: Das Zertifikat am Ende des Pfeils ist Issuer des Zertifikats an der
Spitze des Pfeils).


```
  -----------                                   ----------
 | Root Nord |                                 | Root Süd |
  -----------                                   ----------
       |      \ -----------       ----------- /      |
       |       | Root Süd  |     | Root Nord |       |
       |       | (sig. v.  |     | (sig. v.  |       |
       |       | Root N.)  |     | Root Süd) |       |
       v        -----------  \ /  -----------        v
  -----------                / \                ----------
 | Int. Nord | <-------------   -------------> | Int. Süd |
  -----------                                   ----------
       |                                             |
       v                                             v
  -----------                                   ----------
 | Cert Nord |                                 | Cert Süd |
  -----------                                   ----------
```

Die beiden Zertifikate ```Root Nord``` und ```Root Nord (sig. v. Root Süd)``` 
basieren auf dem gleichen Private Key - d.h. sie enthalten auch den gleichen 
Public Key.
Dieser ist in der Variante ```Root Nord``` mit seinem eigenen Private Key 
signiert - in der Variante ```Root Nord (sig. v. Root Süd)``` ist er mit dem
Private Key von ```Root Süd``` signiert.

Eine kryptografische Signatur nach dem X509-Standard ist ein mit dem signierenden
private Key verschlüsselter Hash-Wert.

Beim Prüfen der Signatur entschlüsselt der Prüfende den verschlüsselten Hash-Wert 
mit dem Public Key des Signierers, und prüft, ob der Hash-Wert mit dem selbst 
berechneten Hash-Wert des Dokuments (in unserem Fall des Zertifikats) übereinstimmt.

https://www.itu.int/rec/dologin_pub.asp?lang=e&id=T-REC-X.509-201210-I!!PDF-E&type=items

## Zertifikats-Ketten

Daraus ergeben sich folgende Zertifikatsketten (Chains) für die Verifizierung
der Zertifikate:

```
Root Nord -> Int. Nord -> Cert Nord
Root Nord -> Root Süd (sig. v. Root N.)  -> Int. Süd  -> Cert Süd

Root Süd  -> Int. Süd  -> Cert Süd
Root Süd  -> Root Nord (sig. v. Root S.) -> Int. Nord -> Cert Nord
```

## Bisherige Chain Files

Bisher haben wir an Clients und Server die folgenden Chain Files ausgegeben:

### Chain File Nord

```
Root Nord
Int. Nord
```

### Chain File Süd

```
Root Süd
Int. Süd Client
Int. Süd Server
```

## Neue Chain Files

Damit Clients und Server die Zertifikate entlang dieser Zertifikatsketten
verifizieren können, müssen in den Chain Files die folgenden Zertifikate
enthalten sein:

### Chain File Nord

```
Root Süd
Int. Süd Client
Int. Süd Server
Root Nord (sig. v. Root S.)
Int. Nord
```

Clients verifizieren Zertifikate entlang dieser Ketten (Zertifikate, denen die
Clients vertrauen sind __fett_ abgebildet:

> Client Süd: ```Cert Nord``` <- ```Int Nord``` <- ```Root Nord``` <- __```Root Süd```__
>
> Client Nord: ```Cert Nord``` <- ```Int Nord``` <- __```Root Nord```__

### Chain File Süd

```
Root Nord
Int. Nord
Root Süd (sig. v. Root N.)
Int. Süd Client
Int Süd Server
```

Clients verifizieren Zertifikate entlang dieser Ketten (Zertifikate, denen die
Clients vertrauen sind __fett__ abgebildet:

> Client Süd: ```Cert Süd``` <- ```Int. Süd``` <- __```Root Süd```__
>
> Client Nord: ```Cert Süd``` <- ```Int. Süd``` <- ```Root Süd``` <- __```Root Nord```__


## Verifizieren von Zertifikaten

Nachrichten aus dem jeweils anderen CA-Bereich lassen sich mit den neuen Chain Files verifizieren.

Innerhalb der bisherigen Hierarchie funktioniert das Verifizieren der Zertifikate
 mit den alten Chains.

Nachrichten aus dem jeweils anderen CA-Bereich lassen sich
mit den bestehenden Chain Files verifizieren, wenn der Kommunikationspartner die Chain mit
dem cross-signierten Root-CA Zertifikat mitsendet.

## Auswirkungen für die Praxis

Cross-Signing wird (temporär) im Rahmen von Mergern von Organisationen verwendet, 
um den Aufwand des Roll-out der (zusätzlichen) Root CA-Zertifikate auf alle 
Clients zu vermeiden.

Da für das Verifizieren der "fremden" Zertifikate eine angepasste Zertifikats-Chain 
erforderlich ist, muss die neue Chain aber auf den zu verifizierenden 
Server-Systemen eingespielt werden.

### Auswirkungen auf TLS Server

TLS Server senden ihre Zertifikats-Chain mit. Damit Clients aus Süd mit Servern aus Nord
kommunizieren können, müssen die Server das Chain File Süd mitschicken.

Clients aus Nord können auch mit diesem Chain File die Zertifikate weiter verifizieren,
da sie dem Root Nord-Zertifikat vertrauen.

Für die entgegegengesetzte Richtung umgekehrt.

### E-Mail: Signieren und Verschlüsseln

???