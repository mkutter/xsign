#!/bin/sh
try() {
  "$@" || (e=$?; echo "$@" > /dev/stderr; exit $e)
}
try rm -rf out
try mkdir out
try /bin/sh -c "echo 01 > out/root1-serial"
try /bin/sh -c "echo 01 > out/root2-serial"
try /bin/sh -c "echo 01 > out/int1-serial"
try /bin/sh -c "echo 01 > out/int2-serial"

touch out/root1-index.txt
touch out/root2-index.txt
touch out/int1-index.txt
touch out/int2-index.txt

# Generate the key
try openssl genrsa -out out/root1.key 2048
try openssl genrsa -out out/root2.key 2048
try openssl genrsa -out out/int1.key 2048
try openssl genrsa -out out/int2.key 2048
try openssl genrsa -out out/crt1.key 2048
try openssl genrsa -out out/crt2.key 2048

# Generate the root1 certificate
try openssl req \
    -new \
    -key out/root1.key \
    -out out/root1.req \
    -subj "/C=DE/O=Mueller Backwaren/CN=Root CA 1" \
    -config ca1.cnf \

try openssl x509 \
    -req -days 3650 \
    -sha256 \
    -in out/root1.req \
    -out out/root1.pem \
    -text \
    -signkey out/root1.key \
    -extfile ca1.cnf \
    -extensions v3_ca 

# generate server1 ca

try openssl req \
    -new \
    -key out/int1.key \
    -out out/int1.req \
    -subj "/C=DE/O=Mueller Backwaren/CN=Int CA 1" \
    -config ca1.cnf \

try openssl ca \
    -config ca1.cnf \
    -extensions v3_intermediate_ca \
    -days 375 \
    -notext \
    -md sha256 \
    -in out/int1.req \
    -out out/int1.pem \
    -notext \
    -batch

#generate crt1 cert

try openssl req \
    -new \
    -key out/crt1.key \
    -out out/crt1.req \
    -subj "/C=DE/O=Mueller Backwaren/CN=Crt 1" \
    -config int1.cnf

try openssl ca \
    -config int1.cnf \
    -extensions server_cert \
    -days 375 \
    -notext \
    -md sha256 \
    -in out/crt1.req \
    -out out/crt1.pem \
    -notext \
    -batch 

# Generate the root2 cert
try openssl req \
    -new \
    -key out/root2.key \
    -out out/root2.req \
    -subj "/C=DE/O=Mueller Backwaren/CN=Root CA 2" \
    -config ca2.cnf

try openssl x509 \
    -req -days 3650 \
    -sha256 \
    -in out/root2.req \
    -out out/root2.pem \
    -text \
    -signkey out/root2.key \
    -extfile ca2.cnf \
    -extensions v3_ca


# generate server2 ca

try openssl req \
    -new \
    -key out/int2.key \
    -out out/int2.req \
    -subj "/C=DE/O=Mueller Backwaren/CN=Int CA 2" \
    -config ca2.cnf

try openssl ca \
    -config ca2.cnf \
    -extensions v3_intermediate_ca \
    -days 375 \
    -notext \
    -md sha256 \
    -in out/int2.req \
    -out out/int2.pem \
    -notext \
    -batch 


#generate crt2 cert

try openssl req \
    -new \
    -key out/crt2.key \
    -out out/crt2.req \
    -subj "/C=DE/O=Mueller Backwaren/CN=Crt 2" \
    -config int2.cnf

try openssl ca \
    -config int2.cnf \
    -extensions server_cert \
    -days 375 \
    -notext \
    -md sha256 \
    -in out/crt2.req \
    -out out/crt2.pem \
    -notext \
    -batch 

## Chains fuer CAs ohne X-Signing
cat out/root1.pem out/int1.pem > out/chain1.pem
cat out/root2.pem out/int2.pem > out/chain2.pem

## X-Signing

# root 1

try openssl x509 \
    -x509toreq \
    -in out/root1.pem \
    -signkey out/root1.key \
    -out out/root1x.req

try openssl ca \
    -config ca2.cnf \
    -extensions v3_ca \
    -days 375 \
    -notext \
    -md sha256 \
    -in out/root1x.req \
    -out out/root1x.pem \
    -notext \
    -batch


# root 2

try openssl x509 \
    -x509toreq \
    -in out/root2.pem \
    -signkey out/root2.key \
    -out out/root2x.req

try openssl ca \
    -config ca1.cnf \
    -extensions v3_ca \
    -days 375 \
    -notext \
    -md sha256 \
    -in out/root2x.req \
    -out out/root2x.pem \
    -notext \
    -batch

## Chains für CAs mit X-Signing

cat out/root1.pem out/int1.pem out/root2x.pem out/int2.pem > out/chain1x.pem
cat out/root2.pem out/int2.pem out/root1x.pem out/int1.pem > out/chain2x.pem

echo "\nVerify mit eigener Chain"

try openssl verify \
    -CAfile out/chain1.pem out/crt1.pem

try openssl verify \
    -CAfile out/chain2.pem out/crt2.pem

echo "\nVerify mit X-Chain"

try openssl verify \
    -CAfile out/chain1x.pem out/crt1.pem out/crt2.pem

try openssl verify -verbose \
    -CAfile out/chain2x.pem out/crt1.pem out/crt2.pem

